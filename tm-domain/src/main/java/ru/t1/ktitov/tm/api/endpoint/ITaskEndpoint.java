package ru.t1.ktitov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktitov.tm.dto.request.task.*;
import ru.t1.ktitov.tm.dto.response.task.*;

public interface ITaskEndpoint {

    @NotNull
    TaskBindToProjectResponse bindTaskToProject(@NotNull final TaskBindToProjectRequest request);

    @NotNull
    TaskChangeStatusByIdResponse changeStatusById(@NotNull final TaskChangeStatusByIdRequest request);

    @NotNull
    TaskChangeStatusByIndexResponse changeStatusByIndex(@NotNull final TaskChangeStatusByIndexRequest request);

    @NotNull
    TaskClearResponse clearTasks(@NotNull final TaskClearRequest request);

    @NotNull
    TaskCreateResponse create(@NotNull final TaskCreateRequest request);

    @NotNull
    TaskListResponse listTasks(@NotNull final TaskListRequest request);

    @NotNull
    TaskRemoveByIdResponse removeById(@NotNull final TaskRemoveByIdRequest request);

    @NotNull
    TaskRemoveByIndexResponse removeByIndex(@NotNull final TaskRemoveByIndexRequest request);

    @NotNull
    TaskGetByIdResponse getById(@NotNull final TaskGetByIdRequest request);

    @NotNull
    TaskGetByIndexResponse getByIndex(@NotNull final TaskGetByIndexRequest request);

    @NotNull
    TaskGetByProjectIdResponse getByProjectId(@NotNull final TaskGetByProjectIdRequest request);

    @NotNull
    TaskUnbindFromProjectResponse unbindTaskFromProject(@NotNull final TaskUnbindFromProjectRequest request);

    @NotNull
    TaskUpdateByIdResponse updateById(@NotNull final TaskUpdateByIdRequest request);

    @NotNull
    TaskUpdateByIndexResponse updateByIndex(@NotNull final TaskUpdateByIndexRequest request);

}
