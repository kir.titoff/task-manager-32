package ru.t1.ktitov.tm.dto.response.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.model.Task;

@Getter
@Setter
@NoArgsConstructor
public final class TaskCreateResponse extends AbstractTaskResponse {

    public TaskCreateResponse(@Nullable final Task task) {
        super(task);
    }

}
