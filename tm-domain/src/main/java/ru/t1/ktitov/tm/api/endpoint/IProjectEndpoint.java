package ru.t1.ktitov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktitov.tm.dto.request.project.*;
import ru.t1.ktitov.tm.dto.response.project.*;

public interface IProjectEndpoint {

    @NotNull
    ProjectChangeStatusByIdResponse changeStatusById(@NotNull final ProjectChangeStatusByIdRequest request);

    @NotNull
    ProjectChangeStatusByIndexResponse changeStatusByIndex(@NotNull final ProjectChangeStatusByIndexRequest request);

    @NotNull
    ProjectClearResponse clearProjects(@NotNull final ProjectClearRequest request);

    @NotNull
    ProjectCreateResponse create(@NotNull final ProjectCreateRequest request);

    @NotNull
    ProjectListResponse listProjects(@NotNull final ProjectListRequest request);

    @NotNull
    ProjectRemoveByIdResponse removeById(@NotNull final ProjectRemoveByIdRequest request);

    @NotNull
    ProjectRemoveByIndexResponse removeByIndex(@NotNull final ProjectRemoveByIndexRequest request);

    @NotNull
    ProjectGetByIdResponse getById(@NotNull final ProjectGetByIdRequest request);

    @NotNull
    ProjectGetByIndexResponse getByIndex(@NotNull final ProjectGetByIndexRequest request);

    @NotNull
    ProjectUpdateByIdResponse updateById(@NotNull final ProjectUpdateByIdRequest request);

    @NotNull
    ProjectUpdateByIndexResponse updateByIndex(@NotNull final ProjectUpdateByIndexRequest request);

}
