package ru.t1.ktitov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktitov.tm.dto.request.data.*;
import ru.t1.ktitov.tm.dto.response.data.*;

public interface IDomainEndpoint {

    @NotNull
    DataBackupLoadResponse loadDataBackup(@NotNull final DataBackupLoadRequest request);

    @NotNull
    DataBackupSaveResponse saveDataBackup(@NotNull final DataBackupSaveRequest request);

    @NotNull
    DataBase64LoadResponse loadDataBase64(@NotNull final DataBase64LoadRequest request);

    @NotNull
    DataBase64SaveResponse saveDataBase64(@NotNull final DataBase64SaveRequest request);

    @NotNull
    DataBinaryLoadResponse loadDataBinary(@NotNull final DataBinaryLoadRequest request);

    @NotNull
    DataBinarySaveResponse saveDataBinary(@NotNull final DataBinarySaveRequest request);

    @NotNull
    DataJsonFasterXmlLoadResponse loadDataJsonFasterXml(@NotNull final DataJsonFasterXmlLoadRequest request);

    @NotNull
    DataJsonFasterXmlSaveResponse saveDataJsonFasterXml(@NotNull final DataJsonFasterXmlSaveRequest request);

    @NotNull
    DataJsonJaxBLoadResponse loadDataJsonJaxB(@NotNull final DataJsonJaxBLoadRequest request);

    @NotNull
    DataJsonJaxBSaveResponse saveDataJsonJaxB(@NotNull final DataJsonJaxBSaveRequest request);

    @NotNull
    DataXmlFasterXmlLoadResponse loadDataXmlFasterXml(@NotNull final DataXmlFasterXmlLoadRequest request);

    @NotNull
    DataXmlFasterXmlSaveResponse saveDataXmlFasterXml(@NotNull final DataXmlFasterXmlSaveRequest request);

    @NotNull
    DataXmlJaxBLoadResponse loadDataXmlJaxB(@NotNull final DataXmlJaxBLoadRequest request);

    @NotNull
    DataXmlJaxBSaveResponse saveDataXmlJaxB(@NotNull final DataXmlJaxBSaveRequest request);

    @NotNull
    DataYamlFasterXmlLoadResponse loadDataYamlFasterXml(@NotNull final DataYamlFasterXmlLoadRequest request);

    @NotNull
    DataYamlFasterXmlSaveResponse saveDataYamlFasterXml(@NotNull final DataYamlFasterXmlSaveRequest request);

}
