package ru.t1.ktitov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktitov.tm.dto.request.user.*;
import ru.t1.ktitov.tm.dto.response.user.*;

public interface IUserEndpoint {

    @NotNull
    UserLockResponse lockUser(@NotNull final UserLockRequest request);

    @NotNull
    UserUnlockResponse unlockUser(@NotNull final UserUnlockRequest request);

    @NotNull
    UserRemoveResponse removeUser(@NotNull final UserRemoveRequest request);

    @NotNull
    UserUpdateProfileResponse updateUserProfile(@NotNull final UserUpdateProfileRequest request);

    @NotNull
    UserChangePasswordResponse changeUserPassword(@NotNull final UserChangePasswordRequest request);

    @NotNull
    UserRegistryResponse registryUser(@NotNull final UserRegistryRequest request);

}
