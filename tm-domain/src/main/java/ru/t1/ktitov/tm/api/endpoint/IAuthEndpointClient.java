package ru.t1.ktitov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktitov.tm.dto.request.user.UserLoginRequest;
import ru.t1.ktitov.tm.dto.request.user.UserLogoutRequest;
import ru.t1.ktitov.tm.dto.request.user.UserProfileRequest;
import ru.t1.ktitov.tm.dto.response.user.UserLoginResponse;
import ru.t1.ktitov.tm.dto.response.user.UserLogoutResponse;
import ru.t1.ktitov.tm.dto.response.user.UserProfileResponse;

public interface IAuthEndpointClient {

    @NotNull
    UserLoginResponse login(@NotNull final UserLoginRequest request);

    @NotNull
    UserLogoutResponse logout(@NotNull final UserLogoutRequest request);

    @NotNull
    UserProfileResponse profile(@NotNull final UserProfileRequest request);

}
