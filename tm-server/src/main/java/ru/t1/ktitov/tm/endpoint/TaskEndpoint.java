package ru.t1.ktitov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.api.endpoint.ITaskEndpoint;
import ru.t1.ktitov.tm.api.service.IProjectTaskService;
import ru.t1.ktitov.tm.api.service.IServiceLocator;
import ru.t1.ktitov.tm.api.service.ITaskService;
import ru.t1.ktitov.tm.dto.request.task.*;
import ru.t1.ktitov.tm.dto.response.task.*;
import ru.t1.ktitov.tm.enumerated.Sort;
import ru.t1.ktitov.tm.enumerated.Status;
import ru.t1.ktitov.tm.model.Task;

import java.util.List;

public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private ITaskService getTaskService() {
        return serviceLocator.getTaskService();
    }

    @NotNull
    private IProjectTaskService getProjectTaskService() {
        return serviceLocator.getProjectTaskService();
    }

    @Override
    @NotNull
    public TaskBindToProjectResponse bindTaskToProject(@NotNull final TaskBindToProjectRequest request) {
        check(request);
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String taskId = request.getTaskId();
        @Nullable final String userId = request.getUserId();
        getProjectTaskService().bindTaskToProject(userId, projectId, taskId);
        return new TaskBindToProjectResponse();
    }

    @Override
    @NotNull
    public TaskChangeStatusByIdResponse changeStatusById(@NotNull final TaskChangeStatusByIdRequest request) {
        check(request);
        @Nullable final String taskId = request.getTaskId();
        @Nullable final Status status = request.getStatus();
        @Nullable final String userId = request.getUserId();
        @NotNull final Task task = getTaskService().changeTaskStatusById(userId, taskId, status);
        return new TaskChangeStatusByIdResponse(task);
    }

    @Override
    @NotNull
    public TaskChangeStatusByIndexResponse changeStatusByIndex(@NotNull final TaskChangeStatusByIndexRequest request) {
        check(request);
        @Nullable final Integer index = request.getTaskIndex() - 1;
        @Nullable final Status status = request.getStatus();
        @Nullable final String userId = request.getUserId();
        @NotNull final Task task = getTaskService().changeTaskStatusByIndex(userId, index, status);
        return new TaskChangeStatusByIndexResponse(task);
    }

    @Override
    @NotNull
    public TaskClearResponse clearTasks(@NotNull final TaskClearRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        getTaskService().clear(userId);
        return new TaskClearResponse();
    }

    @Override
    @NotNull
    public TaskCreateResponse create(@NotNull final TaskCreateRequest request) {
        check(request);
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final String userId = request.getUserId();
        @NotNull final Task task = getTaskService().create(userId, name, description);
        return new TaskCreateResponse(task);
    }

    @Override
    @NotNull
    public TaskListResponse listTasks(@NotNull final TaskListRequest request) {
        check(request);
        @Nullable final Sort sort = request.getSort();
        @Nullable final String userId = request.getUserId();
        @Nullable final List<Task> tasks = getTaskService().findAll(userId, sort);
        return new TaskListResponse(tasks);
    }

    @Override
    @NotNull
    public TaskRemoveByIdResponse removeById(@NotNull final TaskRemoveByIdRequest request) {
        check(request);
        @Nullable final String taskId = request.getTaskId();
        @Nullable final String userId = request.getUserId();
        @NotNull final Task task = getTaskService().removeById(userId, taskId);
        return new TaskRemoveByIdResponse(task);
    }

    @Override
    @NotNull
    public TaskRemoveByIndexResponse removeByIndex(@NotNull final TaskRemoveByIndexRequest request) {
        check(request);
        @Nullable final Integer index = request.getTaskIndex() - 1;
        @Nullable final String userId = request.getUserId();
        @NotNull final Task task = getTaskService().removeByIndex(userId, index);
        return new TaskRemoveByIndexResponse(task);
    }

    @Override
    @NotNull
    public TaskGetByIdResponse getById(@NotNull final TaskGetByIdRequest request) {
        check(request);
        @Nullable final String taskId = request.getTaskId();
        @Nullable final String userId = request.getUserId();
        @NotNull final Task task = getTaskService().findOneById(userId, taskId);
        return new TaskGetByIdResponse(task);
    }

    @Override
    @NotNull
    public TaskGetByIndexResponse getByIndex(@NotNull final TaskGetByIndexRequest request) {
        check(request);
        @Nullable final Integer index = request.getTaskIndex() - 1;
        @Nullable final String userId = request.getUserId();
        @NotNull final Task task = getTaskService().findOneByIndex(userId, index);
        return new TaskGetByIndexResponse(task);
    }

    @Override
    @NotNull
    public TaskGetByProjectIdResponse getByProjectId(@NotNull final TaskGetByProjectIdRequest request) {
        check(request);
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String userId = request.getUserId();
        @NotNull final List<Task> tasks = getTaskService().findAllByProjectId(userId, projectId);
        return new TaskGetByProjectIdResponse(tasks);
    }

    @Override
    @NotNull
    public TaskUnbindFromProjectResponse unbindTaskFromProject(@NotNull final TaskUnbindFromProjectRequest request) {
        check(request);
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String taskId = request.getTaskId();
        @Nullable final String userId = request.getUserId();
        getProjectTaskService().unbindTaskFromProject(userId, projectId, taskId);
        return new TaskUnbindFromProjectResponse();
    }

    @Override
    @NotNull
    public TaskUpdateByIdResponse updateById(@NotNull final TaskUpdateByIdRequest request) {
        check(request);
        @Nullable final String taskId = request.getTaskId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final String userId = request.getUserId();
        @NotNull final Task task = getTaskService().updateById(userId, taskId, name, description);
        return new TaskUpdateByIdResponse(task);
    }

    @Override
    @NotNull
    public TaskUpdateByIndexResponse updateByIndex(@NotNull final TaskUpdateByIndexRequest request) {
        check(request);
        @Nullable final Integer index = request.getTaskIndex() - 1;
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final String userId = request.getUserId();
        @NotNull final Task task = getTaskService().updateByIndex(userId, index, name, description);
        return new TaskUpdateByIndexResponse(task);
    }

}
