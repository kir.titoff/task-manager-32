package ru.t1.ktitov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktitov.tm.api.endpoint.IDomainEndpoint;
import ru.t1.ktitov.tm.api.service.IDomainService;
import ru.t1.ktitov.tm.api.service.IServiceLocator;
import ru.t1.ktitov.tm.dto.request.data.*;
import ru.t1.ktitov.tm.dto.response.data.*;
import ru.t1.ktitov.tm.enumerated.Role;

public final class DomainEndpoint extends AbstractEndpoint implements IDomainEndpoint {

    public DomainEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IDomainService getDomainService() {
        return serviceLocator.getDomainService();
    }

    @Override
    @NotNull
    public DataBackupLoadResponse loadDataBackup(@NotNull final DataBackupLoadRequest request) {
        check(request, Role.ADMIN);
        getDomainService().loadDataBackup();
        return new DataBackupLoadResponse();
    }

    @Override
    @NotNull
    public DataBackupSaveResponse saveDataBackup(@NotNull final DataBackupSaveRequest request) {
        check(request, Role.ADMIN);
        getDomainService().saveDataBackup();
        return new DataBackupSaveResponse();
    }

    @Override
    @NotNull
    public DataBase64LoadResponse loadDataBase64(@NotNull final DataBase64LoadRequest request) {
        check(request, Role.ADMIN);
        getDomainService().loadDataBase64();
        return new DataBase64LoadResponse();
    }

    @Override
    @NotNull
    public DataBase64SaveResponse saveDataBase64(@NotNull final DataBase64SaveRequest request) {
        check(request, Role.ADMIN);
        getDomainService().saveDataBase64();
        return new DataBase64SaveResponse();
    }

    @Override
    @NotNull
    public DataBinaryLoadResponse loadDataBinary(@NotNull final DataBinaryLoadRequest request) {
        check(request, Role.ADMIN);
        getDomainService().loadDataBinary();
        return new DataBinaryLoadResponse();
    }

    @Override
    @NotNull
    public DataBinarySaveResponse saveDataBinary(@NotNull final DataBinarySaveRequest request) {
        check(request, Role.ADMIN);
        getDomainService().saveDataBinary();
        return new DataBinarySaveResponse();
    }

    @Override
    @NotNull
    public DataJsonFasterXmlLoadResponse loadDataJsonFasterXml(@NotNull final DataJsonFasterXmlLoadRequest request) {
        check(request, Role.ADMIN);
        getDomainService().loadDataJsonFasterXml();
        return new DataJsonFasterXmlLoadResponse();
    }

    @Override
    @NotNull
    public DataJsonFasterXmlSaveResponse saveDataJsonFasterXml(@NotNull final DataJsonFasterXmlSaveRequest request) {
        check(request, Role.ADMIN);
        getDomainService().saveDataJsonFasterXml();
        return new DataJsonFasterXmlSaveResponse();
    }

    @Override
    @NotNull
    public DataJsonJaxBLoadResponse loadDataJsonJaxB(@NotNull final DataJsonJaxBLoadRequest request) {
        check(request, Role.ADMIN);
        getDomainService().loadDataJsonJaxB();
        return new DataJsonJaxBLoadResponse();
    }

    @Override
    @NotNull
    public DataJsonJaxBSaveResponse saveDataJsonJaxB(@NotNull final DataJsonJaxBSaveRequest request) {
        check(request, Role.ADMIN);
        getDomainService().saveDataJsonJaxB();
        return new DataJsonJaxBSaveResponse();
    }

    @Override
    @NotNull
    public DataXmlFasterXmlLoadResponse loadDataXmlFasterXml(@NotNull final DataXmlFasterXmlLoadRequest request) {
        check(request, Role.ADMIN);
        getDomainService().loadDataXmlFasterXml();
        return new DataXmlFasterXmlLoadResponse();
    }

    @Override
    @NotNull
    public DataXmlFasterXmlSaveResponse saveDataXmlFasterXml(@NotNull final DataXmlFasterXmlSaveRequest request) {
        check(request, Role.ADMIN);
        getDomainService().saveDataXmlFasterXml();
        return new DataXmlFasterXmlSaveResponse();
    }

    @Override
    @NotNull
    public DataXmlJaxBLoadResponse loadDataXmlJaxB(@NotNull final DataXmlJaxBLoadRequest request) {
        check(request, Role.ADMIN);
        getDomainService().loadDataXmlJaxB();
        return new DataXmlJaxBLoadResponse();
    }

    @Override
    @NotNull
    public DataXmlJaxBSaveResponse saveDataXmlJaxB(@NotNull final DataXmlJaxBSaveRequest request) {
        check(request, Role.ADMIN);
        getDomainService().saveDataXmlJaxB();
        return new DataXmlJaxBSaveResponse();
    }

    @Override
    @NotNull
    public DataYamlFasterXmlLoadResponse loadDataYamlFasterXml(@NotNull final DataYamlFasterXmlLoadRequest request) {
        check(request, Role.ADMIN);
        getDomainService().loadDataYamlFasterXml();
        return new DataYamlFasterXmlLoadResponse();
    }

    @Override
    @NotNull
    public DataYamlFasterXmlSaveResponse saveDataYamlFasterXml(@NotNull final DataYamlFasterXmlSaveRequest request) {
        check(request, Role.ADMIN);
        getDomainService().saveDataYamlFasterXml();
        return new DataYamlFasterXmlSaveResponse();
    }

}
