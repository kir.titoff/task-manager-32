package ru.t1.ktitov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.ktitov.tm.api.service.IProjectService;
import ru.t1.ktitov.tm.api.service.IProjectTaskService;
import ru.t1.ktitov.tm.api.service.IServiceLocator;
import ru.t1.ktitov.tm.dto.request.project.*;
import ru.t1.ktitov.tm.dto.response.project.*;
import ru.t1.ktitov.tm.enumerated.Sort;
import ru.t1.ktitov.tm.enumerated.Status;
import ru.t1.ktitov.tm.model.Project;

import java.util.List;

public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    public ProjectEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IProjectService getProjectService() {
        return serviceLocator.getProjectService();
    }

    @NotNull
    private IProjectTaskService getProjectTaskService() {
        return serviceLocator.getProjectTaskService();
    }

    @Override
    @NotNull
    public ProjectChangeStatusByIdResponse changeStatusById(
            @NotNull final ProjectChangeStatusByIdRequest request
    ) {
        check(request);
        @Nullable final String id = request.getUserId();
        @Nullable final Status status = request.getStatus();
        @Nullable final String userId = request.getUserId();
        @NotNull final Project project = getProjectService().changeProjectStatusById(userId, id, status);
        return new ProjectChangeStatusByIdResponse(project);
    }

    @Override
    @NotNull
    public ProjectChangeStatusByIndexResponse changeStatusByIndex(
            @NotNull final ProjectChangeStatusByIndexRequest request
    ) {
        check(request);
        @Nullable final Integer index = request.getProjectIndex() - 1;
        @Nullable final Status status = request.getStatus();
        @Nullable final String userId = request.getUserId();
        @NotNull final Project project = getProjectService().changeProjectStatusByIndex(userId, index, status);
        return new ProjectChangeStatusByIndexResponse(project);
    }

    @Override
    @NotNull
    public ProjectClearResponse clearProjects(@NotNull final ProjectClearRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        getProjectService().clear(userId);
        return new ProjectClearResponse();
    }

    @Override
    @NotNull
    public ProjectCreateResponse create(@NotNull final ProjectCreateRequest request) {
        check(request);
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final String userId = request.getUserId();
        @NotNull final Project project = getProjectService().create(userId, name, description);
        return new ProjectCreateResponse(project);
    }

    @Override
    @NotNull
    public ProjectListResponse listProjects(@NotNull final ProjectListRequest request) {
        check(request);
        @Nullable final Sort sort = request.getSort();
        @Nullable final String userId = request.getUserId();
        @Nullable final List<Project> projects = getProjectService().findAll(userId, sort);
        return new ProjectListResponse(projects);
    }

    @Override
    @NotNull
    public ProjectRemoveByIdResponse removeById(@NotNull final ProjectRemoveByIdRequest request) {
        check(request);
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String userId = request.getUserId();
        @Nullable final Project project = getProjectTaskService().removeProjectById(userId, projectId);
        return new ProjectRemoveByIdResponse(project);
    }

    @Override
    @NotNull
    public ProjectRemoveByIndexResponse removeByIndex(@NotNull final ProjectRemoveByIndexRequest request) {
        check(request);
        @Nullable final Integer index = request.getProjectIndex() - 1;
        @Nullable final String userId = request.getUserId();
        @NotNull final Project project = getProjectService().findOneByIndex(userId, index);
        getProjectTaskService().removeProjectById(userId, project.getId());
        return new ProjectRemoveByIndexResponse(project);
    }

    @Override
    @NotNull
    public ProjectGetByIdResponse getById(@NotNull final ProjectGetByIdRequest request) {
        check(request);
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String userId = request.getUserId();
        @NotNull final Project project = getProjectService().findOneById(userId, projectId);
        return new ProjectGetByIdResponse(project);
    }

    @Override
    @NotNull
    public ProjectGetByIndexResponse getByIndex(@NotNull final ProjectGetByIndexRequest request) {
        check(request);
        @Nullable final Integer index = request.getProjectIndex() - 1;
        @Nullable final String userId = request.getUserId();
        @NotNull final Project project = getProjectService().findOneByIndex(userId, index);
        return new ProjectGetByIndexResponse(project);
    }

    @Override
    @NotNull
    public ProjectUpdateByIdResponse updateById(@NotNull final ProjectUpdateByIdRequest request) {
        check(request);
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final String userId = request.getUserId();
        @NotNull final Project project = getProjectService().updateById(userId, projectId, name, description);
        return new ProjectUpdateByIdResponse(project);
    }

    @Override
    @NotNull
    public ProjectUpdateByIndexResponse updateByIndex(@NotNull final ProjectUpdateByIndexRequest request) {
        check(request);
        @Nullable final Integer index = request.getProjectIndex() - 1;
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final String userId = request.getUserId();
        @NotNull final Project project = getProjectService().updateByIndex(userId, index, name, description);
        return new ProjectUpdateByIndexResponse(project);
    }

}
