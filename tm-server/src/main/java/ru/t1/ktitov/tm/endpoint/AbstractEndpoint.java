package ru.t1.ktitov.tm.endpoint;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.api.service.IServiceLocator;
import ru.t1.ktitov.tm.api.service.IUserService;
import ru.t1.ktitov.tm.dto.request.AbstractUserRequest;
import ru.t1.ktitov.tm.enumerated.Role;
import ru.t1.ktitov.tm.exception.user.AccessDeniedException;
import ru.t1.ktitov.tm.model.User;

@Getter
public abstract class AbstractEndpoint {

    @NotNull
    protected final IServiceLocator serviceLocator;

    public AbstractEndpoint(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    protected void check(
            @Nullable final AbstractUserRequest request,
            @Nullable final Role role
    ) {
        if (request == null) throw new AccessDeniedException();
        if (role == null) throw new AccessDeniedException();
        @Nullable final String userId = request.getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        @NotNull final IUserService userService = serviceLocator.getUserService();
        @Nullable final User user = userService.findOneById(userId);
        if (user == null) throw new AccessDeniedException();
        @Nullable final Role userRole = user.getRole();
        final boolean check = userRole == role;
        if (!check) throw new AccessDeniedException();
    }

    protected void check(@Nullable final AbstractUserRequest request) {
        if (request == null) throw new AccessDeniedException();
        @Nullable final String userId = request.getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        @NotNull final IUserService userService = serviceLocator.getUserService();
        @Nullable final User user = userService.findOneById(userId);
        if (user == null) throw new AccessDeniedException();
    }

}
