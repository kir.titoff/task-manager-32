package ru.t1.ktitov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.api.endpoint.IUserEndpoint;
import ru.t1.ktitov.tm.api.service.IAuthService;
import ru.t1.ktitov.tm.api.service.IServiceLocator;
import ru.t1.ktitov.tm.api.service.IUserService;
import ru.t1.ktitov.tm.dto.request.user.*;
import ru.t1.ktitov.tm.dto.response.user.*;
import ru.t1.ktitov.tm.enumerated.Role;
import ru.t1.ktitov.tm.model.User;

public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IUserService getUserService() {
        return this.serviceLocator.getUserService();
    }

    @Override
    @NotNull
    public UserLockResponse lockUser(@NotNull final UserLockRequest request) {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        @NotNull final User user = getUserService().lockUserByLogin(login);
        return new UserLockResponse(user);
    }

    @Override
    @NotNull
    public UserUnlockResponse unlockUser(@NotNull final UserUnlockRequest request) {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        @NotNull final User user = getUserService().unlockUserByLogin(login);
        return new UserUnlockResponse(user);
    }

    @Override
    @NotNull
    public UserRemoveResponse removeUser(@NotNull final UserRemoveRequest request) {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        @NotNull final User user = getUserService().removeByLogin(login);
        return new UserRemoveResponse(user);
    }

    @Override
    @NotNull
    public UserUpdateProfileResponse updateUserProfile(@NotNull final UserUpdateProfileRequest request) {
        check(request);
        @Nullable final String firstName = request.getFirstName();
        @Nullable final String lastName = request.getLastName();
        @Nullable final String middleName = request.getMiddleName();
        @Nullable final String userId = request.getUserId();
        @NotNull final User user = getUserService().updateUser(
                userId, firstName, lastName, middleName
        );
        return new UserUpdateProfileResponse(user);
    }

    @Override
    @NotNull
    public UserChangePasswordResponse changeUserPassword(@NotNull final UserChangePasswordRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String password = request.getPassword();
        @NotNull final User user = getUserService().setPassword(userId, password);
        return new UserChangePasswordResponse(user);
    }

    @Override
    @NotNull
    public UserRegistryResponse registryUser(@NotNull final UserRegistryRequest request) {
        @Nullable final String login = request.getLogin();
        @Nullable final String password = request.getPassword();
        @NotNull final String email = request.getEmail();
        @NotNull final IAuthService authService = serviceLocator.getAuthService();
        @NotNull final User user = authService.registry(login, password, email);
        return new UserRegistryResponse(user);
    }

}
