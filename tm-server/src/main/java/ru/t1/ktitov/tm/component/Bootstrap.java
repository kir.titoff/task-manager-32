package ru.t1.ktitov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.api.endpoint.*;
import ru.t1.ktitov.tm.api.repository.IProjectRepository;
import ru.t1.ktitov.tm.api.repository.ITaskRepository;
import ru.t1.ktitov.tm.api.repository.IUserRepository;
import ru.t1.ktitov.tm.api.service.*;
import ru.t1.ktitov.tm.dto.request.data.*;
import ru.t1.ktitov.tm.dto.request.project.*;
import ru.t1.ktitov.tm.dto.request.system.ServerAboutRequest;
import ru.t1.ktitov.tm.dto.request.system.ServerVersionRequest;
import ru.t1.ktitov.tm.dto.request.task.*;
import ru.t1.ktitov.tm.dto.request.user.*;
import ru.t1.ktitov.tm.endpoint.*;
import ru.t1.ktitov.tm.enumerated.Role;
import ru.t1.ktitov.tm.enumerated.Status;
import ru.t1.ktitov.tm.model.Project;
import ru.t1.ktitov.tm.model.User;
import ru.t1.ktitov.tm.repository.ProjectRepository;
import ru.t1.ktitov.tm.repository.TaskRepository;
import ru.t1.ktitov.tm.repository.UserRepository;
import ru.t1.ktitov.tm.service.*;
import ru.t1.ktitov.tm.util.SystemUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@Getter
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IUserService userService = new UserService(propertyService, userRepository, projectRepository, taskRepository);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService);

    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService(this);

    @NotNull
    private final Backup backup = new Backup(this);

    @NotNull
    private final Server server = new Server(this);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    {
        server.registry(ServerAboutRequest.class, systemEndpoint::getAbout);
        server.registry(ServerVersionRequest.class, systemEndpoint::getVersion);

        server.registry(DataBackupLoadRequest.class, domainEndpoint::loadDataBackup);
        server.registry(DataBackupSaveRequest.class, domainEndpoint::saveDataBackup);
        server.registry(DataBase64LoadRequest.class, domainEndpoint::loadDataBase64);
        server.registry(DataBase64SaveRequest.class, domainEndpoint::saveDataBase64);
        server.registry(DataBinaryLoadRequest.class, domainEndpoint::loadDataBinary);
        server.registry(DataBinarySaveRequest.class, domainEndpoint::saveDataBinary);
        server.registry(DataJsonFasterXmlLoadRequest.class, domainEndpoint::loadDataJsonFasterXml);
        server.registry(DataJsonFasterXmlSaveRequest.class, domainEndpoint::saveDataJsonFasterXml);
        server.registry(DataJsonJaxBLoadRequest.class, domainEndpoint::loadDataJsonJaxB);
        server.registry(DataJsonJaxBSaveRequest.class, domainEndpoint::saveDataJsonJaxB);
        server.registry(DataXmlFasterXmlLoadRequest.class, domainEndpoint::loadDataXmlFasterXml);
        server.registry(DataXmlFasterXmlSaveRequest.class, domainEndpoint::saveDataXmlFasterXml);
        server.registry(DataXmlJaxBLoadRequest.class, domainEndpoint::loadDataXmlJaxB);
        server.registry(DataXmlJaxBSaveRequest.class, domainEndpoint::saveDataXmlJaxB);
        server.registry(DataYamlFasterXmlLoadRequest.class, domainEndpoint::loadDataYamlFasterXml);
        server.registry(DataYamlFasterXmlSaveRequest.class, domainEndpoint::saveDataYamlFasterXml);

        server.registry(UserChangePasswordRequest.class, userEndpoint::changeUserPassword);
        server.registry(UserLockRequest.class, userEndpoint::lockUser);
        server.registry(UserRegistryRequest.class, userEndpoint::registryUser);
        server.registry(UserRemoveRequest.class, userEndpoint::removeUser);
        server.registry(UserUnlockRequest.class, userEndpoint::unlockUser);
        server.registry(UserUpdateProfileRequest.class, userEndpoint::updateUserProfile);

        server.registry(ProjectChangeStatusByIdRequest.class, projectEndpoint::changeStatusById);
        server.registry(ProjectChangeStatusByIndexRequest.class, projectEndpoint::changeStatusByIndex);
        server.registry(ProjectClearRequest.class, projectEndpoint::clearProjects);
        server.registry(ProjectCreateRequest.class, projectEndpoint::create);
        server.registry(ProjectGetByIdRequest.class, projectEndpoint::getById);
        server.registry(ProjectGetByIndexRequest.class, projectEndpoint::getByIndex);
        server.registry(ProjectListRequest.class, projectEndpoint::listProjects);
        server.registry(ProjectRemoveByIdRequest.class, projectEndpoint::removeById);
        server.registry(ProjectRemoveByIndexRequest.class, projectEndpoint::removeByIndex);
        server.registry(ProjectUpdateByIdRequest.class, projectEndpoint::updateById);
        server.registry(ProjectUpdateByIndexRequest.class, projectEndpoint::updateByIndex);

        server.registry(TaskBindToProjectRequest.class, taskEndpoint::bindTaskToProject);
        server.registry(TaskChangeStatusByIdRequest.class, taskEndpoint::changeStatusById);
        server.registry(TaskChangeStatusByIndexRequest.class, taskEndpoint::changeStatusByIndex);
        server.registry(TaskClearRequest.class, taskEndpoint::clearTasks);
        server.registry(TaskCreateRequest.class, taskEndpoint::create);
        server.registry(TaskGetByIdRequest.class, taskEndpoint::getById);
        server.registry(TaskGetByIndexRequest.class, taskEndpoint::getByIndex);
        server.registry(TaskGetByProjectIdRequest.class, taskEndpoint::getByProjectId);
        server.registry(TaskListRequest.class, taskEndpoint::listTasks);
        server.registry(TaskRemoveByIdRequest.class, taskEndpoint::removeById);
        server.registry(TaskRemoveByIndexRequest.class, taskEndpoint::removeByIndex);
        server.registry(TaskUnbindFromProjectRequest.class, taskEndpoint::unbindTaskFromProject);
        server.registry(TaskUpdateByIdRequest.class, taskEndpoint::updateById);
        server.registry(TaskUpdateByIndexRequest.class, taskEndpoint::updateByIndex);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void initDemoData() {
        @NotNull final User test = userService.create("test", "test", "test@test.ru");
        @NotNull final User user = userService.create("user", "user", "user@user.ru");
        @NotNull final User admin = userService.create("admin", "admin", Role.ADMIN);

        projectService.add(new Project("TEST", "DESC1", Status.COMPLETED));
        projectService.add(new Project("DEMO", "DESC2", Status.NOT_STARTED));
        projectService.add(new Project("BETA", "DESC3", Status.COMPLETED));
        projectService.add(new Project("KAPPA", "DESC3", Status.IN_PROGRESS));

        taskService.create(test.getId(), "TEST-TASK1", "TASKDESC1");
        taskService.create(test.getId(), "DEMO-TASK2", "TASKDESC2");
        taskService.create(test.getId(), "KAPPA-TASK3", "TASKDESC3");
    }

    private void prepareShutdown() {
        loggerService.info("** TASK MANAGER SERVER IS SHUTTING DOWN **");
        backup.stop();
        server.stop();
    }

    public void run(@Nullable final String[] args) {
        initPID();
        initDemoData();
        loggerService.info("** WELCOME TO TASK MANAGER SERVER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        backup.start();
        server.start();
    }

}
