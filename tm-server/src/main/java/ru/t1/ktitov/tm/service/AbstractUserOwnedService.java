package ru.t1.ktitov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.api.repository.IUserOwnedRepository;
import ru.t1.ktitov.tm.api.service.IUserOwnedService;
import ru.t1.ktitov.tm.enumerated.Sort;
import ru.t1.ktitov.tm.exception.entity.EntityNotFoundException;
import ru.t1.ktitov.tm.exception.field.EmptyIdException;
import ru.t1.ktitov.tm.exception.field.EmptyUserIdException;
import ru.t1.ktitov.tm.exception.field.IncorrectIndexException;
import ru.t1.ktitov.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IUserOwnedRepository<M>>
        extends AbstractService<M, R> implements IUserOwnedService<M> {

    public AbstractUserOwnedService(@NotNull final R repository) {
        super(repository);
    }

    @Nullable
    @Override
    public M add(@Nullable final String userId, @Nullable final M model) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (model == null) throw new EntityNotFoundException();
        return repository.add(userId, model);
    }

    @Override
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        repository.clear(userId);
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.existsById(userId, id);
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return repository.findAll(userId);
    }

    @Nullable
    @Override
    public List<M> findAll(@Nullable final String userId, @Nullable final Comparator comparator) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (comparator == null) return findAll(userId);
        return repository.findAll(userId, comparator);
    }

    @Nullable
    @Override
    public List<M> findAll(@Nullable final String userId, @Nullable final Sort sort) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (sort == null) return findAll(userId);
        return findAll(userId, sort.getComparator());
    }

    @NotNull
    @Override
    public M findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final M model = repository.findOneById(userId, id);
        if (model == null) throw new EntityNotFoundException();
        return model;
    }

    @NotNull
    @Override
    public M findOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        @Nullable final M model = repository.findOneByIndex(userId, index);
        if (model == null) throw new EntityNotFoundException();
        return model;
    }

    @Override
    public int getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return repository.getSize(userId);
    }

    @Nullable
    @Override
    public M remove(@Nullable final String userId, @Nullable final M model) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (model == null) throw new EntityNotFoundException();
        return repository.remove(userId, model);
    }

    @NotNull
    @Override
    public M removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.removeById(userId, id);
    }

    @NotNull
    @Override
    public M removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (index >= repository.getSize()) throw new IncorrectIndexException();
        return repository.removeByIndex(userId, index);
    }

}
