package ru.t1.ktitov.tm.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.api.service.IAuthService;
import ru.t1.ktitov.tm.api.service.IUserService;
import ru.t1.ktitov.tm.component.Server;
import ru.t1.ktitov.tm.dto.request.AbstractRequest;
import ru.t1.ktitov.tm.dto.request.AbstractUserRequest;
import ru.t1.ktitov.tm.dto.request.user.UserLoginRequest;
import ru.t1.ktitov.tm.dto.request.user.UserLogoutRequest;
import ru.t1.ktitov.tm.dto.request.user.UserProfileRequest;
import ru.t1.ktitov.tm.dto.response.AbstractResponse;
import ru.t1.ktitov.tm.dto.response.ApplicationErrorResponse;
import ru.t1.ktitov.tm.dto.response.user.UserLoginResponse;
import ru.t1.ktitov.tm.dto.response.user.UserLogoutResponse;
import ru.t1.ktitov.tm.dto.response.user.UserProfileResponse;
import ru.t1.ktitov.tm.model.User;

import java.io.*;
import java.net.Socket;

public final class ServerRequestTask extends AbstractServerSocketTask {

    @Nullable
    private AbstractResponse response;

    @Nullable
    private AbstractRequest request;

    public ServerRequestTask(
            @NotNull final Server server,
            @NotNull final Socket socket
    ) {
        super(server, socket);
    }

    public ServerRequestTask(
            @NotNull final Server server,
            @NotNull final Socket socket,
            @Nullable final String userId
    ) {
        super(server, socket, userId);
    }

    @Override
    @SneakyThrows
    public void run() {
        processInput();

        processUserId();
        processLogin();
        processProfile();
        processLogout();

        processOperation();
        processOutput();
    }

    private void processInput() throws IOException, ClassNotFoundException {
        @NotNull final InputStream inputStream = socket.getInputStream();
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
        @NotNull final Object object = objectInputStream.readObject();
        request = (AbstractRequest) object;
    }

    private void processUserId() {
        if (!(request instanceof AbstractUserRequest)) return;
        @NotNull final AbstractUserRequest abstractUserRequest = (AbstractUserRequest) request;
        abstractUserRequest.setUserId(userId);
    }

    private void processLogin() {
        if (response != null) return;
        if (!(request instanceof UserLoginRequest)) return;
        try {
            @NotNull final UserLoginRequest userLoginRequest = (UserLoginRequest) request;
            @Nullable final String login = userLoginRequest.getLogin();
            @Nullable final String password = userLoginRequest.getPassword();
            @NotNull final IAuthService authService = server.getServiceLocator().getAuthService();
            @NotNull final User user = authService.check(login, password);
            userId = user.getId();
            response = new UserLoginResponse();
        } catch (@NotNull final Exception e) {
            response = new UserLoginResponse(e);
        }
    }

    private void processProfile() {
        if (response != null) return;
        if (!(request instanceof UserProfileRequest)) return;
        if (userId == null) {
            response = new UserProfileResponse();
            return;
        }
        @NotNull final IUserService userService = server.getServiceLocator().getUserService();
        @NotNull final User user = userService.findOneById(userId);
        response = new UserProfileResponse(user);
    }

    private void processLogout() {
        if (response != null) return;
        if (!(request instanceof UserLogoutRequest)) return;
        userId = null;
        response = new UserLogoutResponse();
    }

    private void processOperation() {
        if (response != null) return;
        try {
            @Nullable final Object result = server.call(request);
            response = (AbstractResponse) result;
        } catch (Exception e) {
            response = new ApplicationErrorResponse(e);
        }
    }

    private void processOutput() throws IOException {
        @NotNull final OutputStream outputStream = socket.getOutputStream();
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
        objectOutputStream.writeObject(response);
        server.submit(new ServerRequestTask(server, socket, userId));
    }

}
