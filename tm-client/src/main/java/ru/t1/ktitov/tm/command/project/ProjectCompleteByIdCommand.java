package ru.t1.ktitov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktitov.tm.dto.request.project.ProjectChangeStatusByIdRequest;
import ru.t1.ktitov.tm.enumerated.Status;
import ru.t1.ktitov.tm.util.TerminalUtil;

public final class ProjectCompleteByIdCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-complete-by-id";

    @NotNull
    public static final String DESCRIPTION = "Complete project by id";

    @Override
    public void execute() {
        System.out.println("[COMPLETE PROJECT BY ID]");
        System.out.print("ENTER ID: ");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest();
        request.setProjectId(id);
        request.setStatus(Status.COMPLETED);
        getProjectEndpointClient().changeStatusById(request);

    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
