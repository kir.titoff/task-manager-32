package ru.t1.ktitov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.ktitov.tm.dto.request.data.DataJsonJaxBLoadRequest;

public class DataJsonJaxBLoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-json-jaxb";

    @NotNull
    public static final String DESCRIPTION = "Load data from json file by jaxb";

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull DataJsonJaxBLoadRequest request = new DataJsonJaxBLoadRequest();
        getDomainEndpointClient().loadDataJsonJaxB(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
