package ru.t1.ktitov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktitov.tm.dto.request.user.UserUpdateProfileRequest;
import ru.t1.ktitov.tm.enumerated.Role;
import ru.t1.ktitov.tm.util.TerminalUtil;

public final class UserUpdateProfileCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "user-update-profile";

    @NotNull
    public static final String DESCRIPTION = "Update profile of current user";

    @Override
    public void execute() {
        System.out.println("[UPDATE USER PROFILE]");
        System.out.print("ENTER FIRST NAME: ");
        @NotNull final String firstName = TerminalUtil.nextLine();
        System.out.print("ENTER LAST NAME: ");
        @NotNull final String lastName = TerminalUtil.nextLine();
        System.out.print("ENTER MIDDLE NAME: ");
        @NotNull final String middleName = TerminalUtil.nextLine();
        @NotNull final UserUpdateProfileRequest request = new UserUpdateProfileRequest();
        request.setFirstName(firstName);
        request.setLastName(lastName);
        request.setMiddleName(middleName);
        getUserEndpointClient().updateUserProfile(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
