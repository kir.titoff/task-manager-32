package ru.t1.ktitov.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.ktitov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.ktitov.tm.dto.request.project.*;
import ru.t1.ktitov.tm.dto.request.user.UserLoginRequest;
import ru.t1.ktitov.tm.dto.request.user.UserLogoutRequest;
import ru.t1.ktitov.tm.dto.request.user.UserProfileRequest;
import ru.t1.ktitov.tm.dto.response.project.*;

@NoArgsConstructor
public final class ProjectEndpointClient extends AbstractEndpointClient implements IProjectEndpoint {

    public ProjectEndpointClient(@NotNull final AbstractEndpointClient client) {
        super(client);
    }

    @SneakyThrows
    public static void main(String[] args) {
        @NotNull final AuthEndpointClient authEndpointClient = new AuthEndpointClient();
        authEndpointClient.connect();
        System.out.println(authEndpointClient.login(new UserLoginRequest("test", "test")).getSuccess());
        System.out.println(authEndpointClient.profile(new UserProfileRequest()).getUser().getEmail());

        @NotNull final ProjectEndpointClient projectClient = new ProjectEndpointClient(authEndpointClient);
        System.out.println(projectClient.create(new ProjectCreateRequest("CLIENTTEST", "test")));
        System.out.println(projectClient.listProjects(new ProjectListRequest()).getProjects());

        System.out.println(authEndpointClient.logout(new UserLogoutRequest()));
        authEndpointClient.disconnect();
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectChangeStatusByIdResponse changeStatusById(@NotNull final ProjectChangeStatusByIdRequest request) {
        return call(request, ProjectChangeStatusByIdResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectChangeStatusByIndexResponse changeStatusByIndex(@NotNull final ProjectChangeStatusByIndexRequest request) {
        return call(request, ProjectChangeStatusByIndexResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectClearResponse clearProjects(@NotNull final ProjectClearRequest request) {
        return call(request, ProjectClearResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectCreateResponse create(@NotNull final ProjectCreateRequest request) {
        return call(request, ProjectCreateResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectListResponse listProjects(@NotNull final ProjectListRequest request) {
        return call(request, ProjectListResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectRemoveByIdResponse removeById(@NotNull final ProjectRemoveByIdRequest request) {
        return call(request, ProjectRemoveByIdResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectRemoveByIndexResponse removeByIndex(@NotNull final ProjectRemoveByIndexRequest request) {
        return call(request, ProjectRemoveByIndexResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectGetByIdResponse getById(@NotNull final ProjectGetByIdRequest request) {
        return call(request, ProjectGetByIdResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectGetByIndexResponse getByIndex(@NotNull final ProjectGetByIndexRequest request) {
        return call(request, ProjectGetByIndexResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectUpdateByIdResponse updateById(@NotNull final ProjectUpdateByIdRequest request) {
        return call(request, ProjectUpdateByIdResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectUpdateByIndexResponse updateByIndex(@NotNull final ProjectUpdateByIndexRequest request) {
        return call(request, ProjectUpdateByIndexResponse.class);
    }

}
