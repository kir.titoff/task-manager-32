package ru.t1.ktitov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktitov.tm.dto.request.project.ProjectUpdateByIdRequest;
import ru.t1.ktitov.tm.util.TerminalUtil;

public final class ProjectUpdateByIdCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-update-by-id";

    @NotNull
    public static final String DESCRIPTION = "Update project by id";

    @Override
    public void execute() {
        System.out.println("[UPDATE PROJECT BY ID]");
        System.out.print("ENTER ID: ");
        @NotNull final String id = TerminalUtil.nextLine();
        System.out.print("ENTER NAME: ");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.print("ENTER DESCRIPTION: ");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final ProjectUpdateByIdRequest request = new ProjectUpdateByIdRequest();
        request.setProjectId(id);
        request.setName(name);
        request.setDescription(description);
        getProjectEndpointClient().updateById(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
