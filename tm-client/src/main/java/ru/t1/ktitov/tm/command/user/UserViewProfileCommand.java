package ru.t1.ktitov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.dto.request.user.UserProfileRequest;
import ru.t1.ktitov.tm.enumerated.Role;
import ru.t1.ktitov.tm.model.User;

public final class UserViewProfileCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "user-view-profile";

    @NotNull
    public static final String DESCRIPTION = "View profile of current user";

    @Override
    public void execute() {
        @NotNull final UserProfileRequest request = new UserProfileRequest();
        @Nullable final User user = getUserEndpointClient().profile(request).getUser();
        System.out.println("CURRENT USER:");
        showUser(user);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
