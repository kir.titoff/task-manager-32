package ru.t1.ktitov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.dto.request.user.UserLoginRequest;
import ru.t1.ktitov.tm.dto.response.user.UserLoginResponse;
import ru.t1.ktitov.tm.enumerated.Role;
import ru.t1.ktitov.tm.util.TerminalUtil;

public final class UserLoginCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "login";

    @NotNull
    public static final String DESCRIPTION = "User login";

    @Override
    public void execute() {
        System.out.println("[USER LOGIN]");
        System.out.print("ENTER LOGIN: ");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.print("ENTER PASSWORD: ");
        @NotNull final String password = TerminalUtil.nextLine();
        @NotNull final UserLoginRequest request = new UserLoginRequest();
        request.setLogin(login);
        request.setPassword(password);
        @NotNull final UserLoginResponse response = getUserEndpointClient().login(request);
        if (!response.getSuccess()) {
            System.out.println("[ERROR]");
            System.out.println(response.getMessage());
        }
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
