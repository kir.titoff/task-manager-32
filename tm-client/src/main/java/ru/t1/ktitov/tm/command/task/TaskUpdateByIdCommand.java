package ru.t1.ktitov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktitov.tm.dto.request.task.TaskUpdateByIdRequest;
import ru.t1.ktitov.tm.util.TerminalUtil;

public final class TaskUpdateByIdCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-update-by-id";

    @NotNull
    public static final String DESCRIPTION = "Update task by id";

    @Override
    public void execute() {
        System.out.println("[UPDATE TASK BY ID]");
        System.out.print("ENTER ID: ");
        @NotNull final String id = TerminalUtil.nextLine();
        System.out.print("ENTER NAME: ");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.print("ENTER DESCRIPTION: ");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final TaskUpdateByIdRequest request = new TaskUpdateByIdRequest();
        request.setTaskId(id);
        request.setName(name);
        request.setDescription(description);
        getTaskEndpointClient().updateById(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
