package ru.t1.ktitov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktitov.tm.dto.request.project.ProjectChangeStatusByIndexRequest;
import ru.t1.ktitov.tm.enumerated.Status;
import ru.t1.ktitov.tm.util.TerminalUtil;

public final class ProjectStartByIndexCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-start-by-index";

    @NotNull
    public static final String DESCRIPTION = "Start project by index";

    @Override
    public void execute() {
        System.out.println("[START PROJECT BY INDEX]");
        System.out.print("ENTER INDEX: ");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final ProjectChangeStatusByIndexRequest request = new ProjectChangeStatusByIndexRequest();
        request.setProjectIndex(index);
        request.setStatus(Status.IN_PROGRESS);
        getProjectEndpointClient().changeStatusByIndex(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
