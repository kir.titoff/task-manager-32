package ru.t1.ktitov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.dto.request.project.ProjectListRequest;
import ru.t1.ktitov.tm.dto.response.project.ProjectListResponse;
import ru.t1.ktitov.tm.enumerated.Sort;
import ru.t1.ktitov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.ktitov.tm.model.Project;
import ru.t1.ktitov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectListCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-list";

    @NotNull
    public static final String DESCRIPTION = "Show all projects";

    @Override
    public void execute() {
        System.out.println("[PROJECT LIST]");
        System.out.println(Arrays.toString(Sort.values()));
        System.out.print("ENTER SORT: ");
        @NotNull final String sortType = TerminalUtil.nextLine();
        @Nullable final Sort sort = Sort.toSort(sortType);
        @NotNull final ProjectListRequest request = new ProjectListRequest();
        request.setSort(sort);
        @NotNull final ProjectListResponse response = getProjectEndpointClient().listProjects(request);
        @Nullable final List<Project> projects = response.getProjects();
        if (projects == null) throw new ProjectNotFoundException();
        int index = 1;
        for (@Nullable final Project project : projects) {
            if (project == null) continue;
            System.out.println(index + ". " + project);
            index++;
        }
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
