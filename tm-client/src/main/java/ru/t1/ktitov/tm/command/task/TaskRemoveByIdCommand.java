package ru.t1.ktitov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktitov.tm.dto.request.task.TaskRemoveByIdRequest;
import ru.t1.ktitov.tm.util.TerminalUtil;

public final class TaskRemoveByIdCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-remove-by-id";

    @NotNull
    public static final String DESCRIPTION = "Remove task by id";

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK BY ID]");
        System.out.print("ENTER ID: ");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final TaskRemoveByIdRequest request = new TaskRemoveByIdRequest();
        request.setTaskId(id);
        getTaskEndpointClient().removeById(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
