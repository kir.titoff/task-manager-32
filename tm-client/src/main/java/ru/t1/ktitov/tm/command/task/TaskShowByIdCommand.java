package ru.t1.ktitov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.dto.request.task.TaskGetByIdRequest;
import ru.t1.ktitov.tm.model.Task;
import ru.t1.ktitov.tm.util.TerminalUtil;

public final class TaskShowByIdCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-show-by-id";

    @NotNull
    public static final String DESCRIPTION = "Show task by id";

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY ID]");
        System.out.print("ENTER ID: ");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final TaskGetByIdRequest request = new TaskGetByIdRequest();
        request.setTaskId(id);
        @Nullable final Task task = getTaskEndpointClient().getById(request).getTask();
        showTask(task);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
