package ru.t1.ktitov.tm.client;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.ktitov.tm.api.endpoint.ISystemEndpointClient;
import ru.t1.ktitov.tm.dto.request.system.ServerAboutRequest;
import ru.t1.ktitov.tm.dto.request.system.ServerVersionRequest;
import ru.t1.ktitov.tm.dto.response.system.ServerAboutResponse;
import ru.t1.ktitov.tm.dto.response.system.ServerVersionResponse;

public final class SystemEndpointClient extends AbstractEndpointClient implements ISystemEndpointClient {

    @SneakyThrows
    public static void main(String[] args) {
        final SystemEndpointClient client = new SystemEndpointClient();
        client.connect();
        final ServerAboutResponse serverAboutResponse = client.getAbout(new ServerAboutRequest());
        System.out.println(serverAboutResponse.getEmail());
        System.out.println(serverAboutResponse.getName());
        final ServerVersionResponse serverVersionResponse = client.getVersion(new ServerVersionRequest());
        System.out.println(serverVersionResponse.getVersion());
    }

    @NotNull
    @SneakyThrows
    public ServerAboutResponse getAbout(@NotNull final ServerAboutRequest request) {
        return call(request, ServerAboutResponse.class);
    }

    @NotNull
    @SneakyThrows
    public ServerVersionResponse getVersion(@NotNull final ServerVersionRequest request) {
        return call(request, ServerVersionResponse.class);
    }

}
