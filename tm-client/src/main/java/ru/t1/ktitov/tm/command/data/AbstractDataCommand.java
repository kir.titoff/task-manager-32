package ru.t1.ktitov.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.api.endpoint.IDomainEndpoint;
import ru.t1.ktitov.tm.command.AbstractCommand;
import ru.t1.ktitov.tm.enumerated.Role;

public abstract class AbstractDataCommand extends AbstractCommand {

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @NotNull
    protected IDomainEndpoint getDomainEndpointClient() {
        return serviceLocator.getDomainEndpointClient();
    }

}
