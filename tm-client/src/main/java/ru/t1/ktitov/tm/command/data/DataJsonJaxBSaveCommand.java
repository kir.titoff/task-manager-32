package ru.t1.ktitov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.ktitov.tm.dto.request.data.DataJsonJaxBSaveRequest;

public class DataJsonJaxBSaveCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-save-json-jaxb";

    @NotNull
    public static final String DESCRIPTION = "Save data in json file by jaxb";

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull DataJsonJaxBSaveRequest request = new DataJsonJaxBSaveRequest();
        getDomainEndpointClient().saveDataJsonJaxB(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
