package ru.t1.ktitov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.ktitov.tm.dto.request.data.DataYamlFasterXmlLoadRequest;

public class DataYamlFasterXmlLoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-yaml-fasterxml";

    @NotNull
    public static final String DESCRIPTION = "Load data from yaml file by fasterxml";

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull DataYamlFasterXmlLoadRequest request = new DataYamlFasterXmlLoadRequest();
        getDomainEndpointClient().loadDataYamlFasterXml(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
