package ru.t1.ktitov.tm.client;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.api.IEndpointClient;
import ru.t1.ktitov.tm.dto.request.AbstractRequest;
import ru.t1.ktitov.tm.dto.response.ApplicationErrorResponse;

import java.io.*;
import java.net.Socket;

@Getter
@Setter
public abstract class AbstractEndpointClient implements IEndpointClient {

    @NotNull
    private String host = "localhost";

    @NotNull
    private Integer port = 6060;

    @Nullable
    private Socket socket;

    public AbstractEndpointClient() {
    }

    public AbstractEndpointClient(@NotNull final String host, @NotNull final Integer port) {
        this.host = host;
        this.port = port;
    }

    public AbstractEndpointClient(@NotNull final AbstractEndpointClient client) {
        this.host = client.host;
        this.port = client.port;
        this.socket = client.socket;
    }

    @NotNull
    @Override
    public Object call(@NotNull final Object object) throws IOException, ClassNotFoundException {
        getObjectOutputStream().writeObject(object);
        return getObjectInputStream().readObject();
    }

    @NotNull
    public <T> T call(@Nullable final AbstractRequest request, @NotNull Class<T> classResponse) throws IOException, ClassNotFoundException {
        getObjectOutputStream().writeObject(request);
        @NotNull final Object response = getObjectInputStream().readObject();
        if (response instanceof ApplicationErrorResponse) {
            @NotNull ApplicationErrorResponse errorResponse = (ApplicationErrorResponse) response;
            throw new RuntimeException(errorResponse.getMessage());
        }
        return (T) response;
    }

    @NotNull
    private ObjectOutputStream getObjectOutputStream() throws IOException {
        return new ObjectOutputStream(getOutputStream());
    }

    @NotNull
    private ObjectInputStream getObjectInputStream() throws IOException {
        return new ObjectInputStream(getInputStream());
    }

    @Nullable
    private OutputStream getOutputStream() throws IOException {
        if (socket == null) return null;
        return socket.getOutputStream();
    }

    @Nullable
    private InputStream getInputStream() throws IOException {
        if (socket == null) return null;
        return socket.getInputStream();
    }

    @NotNull
    @Override
    public Socket connect() throws IOException {
        return socket = new Socket(host, port);
    }

    @Nullable
    @Override
    public Socket disconnect() throws IOException {
        if (socket != null) socket.close();
        return socket;
    }

}
