package ru.t1.ktitov.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.ktitov.tm.api.endpoint.ITaskEndpoint;
import ru.t1.ktitov.tm.dto.request.task.*;
import ru.t1.ktitov.tm.dto.response.task.*;

@NoArgsConstructor
public final class TaskEndpointClient extends AbstractEndpointClient implements ITaskEndpoint {

    public TaskEndpointClient(@NotNull final AbstractEndpointClient client) {
        super(client);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskBindToProjectResponse bindTaskToProject(@NotNull final TaskBindToProjectRequest request) {
        return call(request, TaskBindToProjectResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskChangeStatusByIdResponse changeStatusById(@NotNull final TaskChangeStatusByIdRequest request) {
        return call(request, TaskChangeStatusByIdResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskChangeStatusByIndexResponse changeStatusByIndex(@NotNull final TaskChangeStatusByIndexRequest request) {
        return call(request, TaskChangeStatusByIndexResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskClearResponse clearTasks(@NotNull final TaskClearRequest request) {
        return call(request, TaskClearResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskCreateResponse create(@NotNull final TaskCreateRequest request) {
        return call(request, TaskCreateResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskListResponse listTasks(@NotNull final TaskListRequest request) {
        return call(request, TaskListResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskRemoveByIdResponse removeById(@NotNull final TaskRemoveByIdRequest request) {
        return call(request, TaskRemoveByIdResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskRemoveByIndexResponse removeByIndex(@NotNull final TaskRemoveByIndexRequest request) {
        return call(request, TaskRemoveByIndexResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskGetByIdResponse getById(@NotNull final TaskGetByIdRequest request) {
        return call(request, TaskGetByIdResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskGetByIndexResponse getByIndex(@NotNull final TaskGetByIndexRequest request) {
        return call(request, TaskGetByIndexResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskGetByProjectIdResponse getByProjectId(@NotNull final TaskGetByProjectIdRequest request) {
        return call(request, TaskGetByProjectIdResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskUnbindFromProjectResponse unbindTaskFromProject(@NotNull final TaskUnbindFromProjectRequest request) {
        return call(request, TaskUnbindFromProjectResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskUpdateByIdResponse updateById(@NotNull final TaskUpdateByIdRequest request) {
        return call(request, TaskUpdateByIdResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskUpdateByIndexResponse updateByIndex(@NotNull final TaskUpdateByIndexRequest request) {
        return call(request, TaskUpdateByIndexResponse.class);
    }

}
