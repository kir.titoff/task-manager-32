package ru.t1.ktitov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.ktitov.tm.dto.request.data.DataJsonFasterXmlSaveRequest;

public class DataJsonFasterXmlSaveCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-save-json-fasterxml";

    @NotNull
    public static final String DESCRIPTION = "Save data in json file by fasterxml";

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull DataJsonFasterXmlSaveRequest request = new DataJsonFasterXmlSaveRequest();
        getDomainEndpointClient().saveDataJsonFasterXml(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
