package ru.t1.ktitov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.dto.request.task.TaskGetByIndexRequest;
import ru.t1.ktitov.tm.model.Task;
import ru.t1.ktitov.tm.util.TerminalUtil;

public final class TaskShowByIndexCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-show-by-index";

    @NotNull
    public static final String DESCRIPTION = "Show task by index";

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY INDEX]");
        System.out.print("ENTER INDEX: ");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final TaskGetByIndexRequest request = new TaskGetByIndexRequest();
        request.setTaskIndex(index);
        @Nullable final Task task = getTaskEndpointClient().getByIndex(request).getTask();
        showTask(task);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
