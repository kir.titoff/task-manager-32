package ru.t1.ktitov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.ktitov.tm.dto.request.data.DataJsonFasterXmlLoadRequest;

public class DataJsonFasterXmlLoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-json-fasterxml";

    @NotNull
    public static final String DESCRIPTION = "Load data from json file by fasterxml";

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull DataJsonFasterXmlLoadRequest request = new DataJsonFasterXmlLoadRequest();
        getDomainEndpointClient().loadDataJsonFasterXml(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
