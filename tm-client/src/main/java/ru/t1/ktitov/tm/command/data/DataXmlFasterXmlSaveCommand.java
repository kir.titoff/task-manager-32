package ru.t1.ktitov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.ktitov.tm.dto.request.data.DataXmlFasterXmlSaveRequest;

public class DataXmlFasterXmlSaveCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-save-xml-fasterxml";

    @NotNull
    public static final String DESCRIPTION = "Save data in xml file by fasterxml";

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull DataXmlFasterXmlSaveRequest request = new DataXmlFasterXmlSaveRequest();
        getDomainEndpointClient().saveDataXmlFasterXml(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
