package ru.t1.ktitov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.ktitov.tm.dto.request.data.DataYamlFasterXmlSaveRequest;

public class DataYamlFasterXmlSaveCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-save-jaml-fasterxml";

    @NotNull
    public static final String DESCRIPTION = "Save data in jaml file by fasterxml";

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull DataYamlFasterXmlSaveRequest request = new DataYamlFasterXmlSaveRequest();
        getDomainEndpointClient().saveDataYamlFasterXml(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
