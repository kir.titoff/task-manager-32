package ru.t1.ktitov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktitov.tm.dto.request.user.UserLogoutRequest;
import ru.t1.ktitov.tm.enumerated.Role;

public final class UserLogoutCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "logout";

    @NotNull
    public static final String DESCRIPTION = "User logout";

    @Override
    public void execute() {
        System.out.println("[LOGOUT]");
        @NotNull final UserLogoutRequest request = new UserLogoutRequest();
        getUserEndpointClient().logout(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
