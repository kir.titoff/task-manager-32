package ru.t1.ktitov.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.ktitov.tm.api.endpoint.IAuthEndpointClient;
import ru.t1.ktitov.tm.dto.request.user.UserLoginRequest;
import ru.t1.ktitov.tm.dto.request.user.UserLogoutRequest;
import ru.t1.ktitov.tm.dto.request.user.UserProfileRequest;
import ru.t1.ktitov.tm.dto.response.user.UserLoginResponse;
import ru.t1.ktitov.tm.dto.response.user.UserLogoutResponse;
import ru.t1.ktitov.tm.dto.response.user.UserProfileResponse;

@NoArgsConstructor
public final class AuthEndpointClient extends AbstractEndpointClient implements IAuthEndpointClient {

    public AuthEndpointClient(@NotNull final AbstractEndpointClient client) {
        super(client);
    }

    @SneakyThrows
    public static void main(String[] args) {
        @NotNull final AuthEndpointClient authEndpointClient = new AuthEndpointClient();
        authEndpointClient.connect();
        System.out.println(authEndpointClient.profile(new UserProfileRequest()).getUser());
        System.out.println(authEndpointClient.login(new UserLoginRequest("test", "nottest")).getSuccess());
        System.out.println(authEndpointClient.login(new UserLoginRequest("test", "test")).getSuccess());
        System.out.println(authEndpointClient.profile(new UserProfileRequest()).getUser().getEmail());
        System.out.println(authEndpointClient.logout(new UserLogoutRequest()));
        System.out.println(authEndpointClient.profile(new UserProfileRequest()).getUser());
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserLoginResponse login(@NotNull final UserLoginRequest request) {
        return call(request, UserLoginResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserLogoutResponse logout(@NotNull final UserLogoutRequest request) {
        return call(request, UserLogoutResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserProfileResponse profile(@NotNull final UserProfileRequest request) {
        return call(request, UserProfileResponse.class);
    }

}
