package ru.t1.ktitov.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.ktitov.tm.api.endpoint.IUserEndpoint;
import ru.t1.ktitov.tm.dto.request.user.*;
import ru.t1.ktitov.tm.dto.response.user.*;

@NoArgsConstructor
public final class UserEndpointClient extends AbstractEndpointClient implements IUserEndpoint {

    public UserEndpointClient(@NotNull final AbstractEndpointClient client) {
        super(client);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserLockResponse lockUser(@NotNull final UserLockRequest request) {
        return call(request, UserLockResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserUnlockResponse unlockUser(@NotNull final UserUnlockRequest request) {
        return call(request, UserUnlockResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserRemoveResponse removeUser(@NotNull final UserRemoveRequest request) {
        return call(request, UserRemoveResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserUpdateProfileResponse updateUserProfile(@NotNull final UserUpdateProfileRequest request) {
        return call(request, UserUpdateProfileResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserChangePasswordResponse changeUserPassword(@NotNull final UserChangePasswordRequest request) {
        return call(request, UserChangePasswordResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserRegistryResponse registryUser(@NotNull final UserRegistryRequest request) {
        return call(request, UserRegistryResponse.class);
    }

    @NotNull
    @SneakyThrows
    public UserLoginResponse login(@NotNull final UserLoginRequest request) {
        return call(request, UserLoginResponse.class);
    }

    @NotNull
    @SneakyThrows
    public UserProfileResponse profile(@NotNull final UserProfileRequest request) {
        return call(request, UserProfileResponse.class);
    }

    @NotNull
    @SneakyThrows
    public UserLogoutResponse logout(@NotNull final UserLogoutRequest request) {
        return call(request, UserLogoutResponse.class);
    }


}
