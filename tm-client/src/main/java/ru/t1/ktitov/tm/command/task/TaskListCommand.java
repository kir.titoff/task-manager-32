package ru.t1.ktitov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.dto.request.task.TaskListRequest;
import ru.t1.ktitov.tm.enumerated.Sort;
import ru.t1.ktitov.tm.model.Task;
import ru.t1.ktitov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class TaskListCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-list";

    @NotNull
    public static final String DESCRIPTION = "Show all tasks";

    @Override
    public void execute() {
        System.out.println("[TASK LIST]");
        System.out.println(Arrays.toString(Sort.values()));
        System.out.print("ENTER SORT: ");
        @NotNull final String sortType = TerminalUtil.nextLine();
        @Nullable final Sort sort = Sort.toSort(sortType);
        @NotNull final TaskListRequest request = new TaskListRequest();
        request.setSort(sort);
        @Nullable final List<Task> tasks = getTaskEndpointClient().listTasks(request).getTasks();
        renderTasks(tasks);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
