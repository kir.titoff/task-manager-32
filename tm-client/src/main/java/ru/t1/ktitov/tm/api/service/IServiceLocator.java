package ru.t1.ktitov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktitov.tm.api.IEndpointClient;
import ru.t1.ktitov.tm.client.*;

public interface IServiceLocator {

    @NotNull
    ICommandService getCommandService();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    IEndpointClient getConnectionEndpointClient();

    @NotNull
    ProjectEndpointClient getProjectEndpointClient();

    @NotNull
    TaskEndpointClient getTaskEndpointClient();

    @NotNull
    UserEndpointClient getUserEndpointClient();

    @NotNull
    AuthEndpointClient getAuthEndpointClient();

    @NotNull
    DomainEndpointClient getDomainEndpointClient();

    @NotNull
    SystemEndpointClient getSystemEndpointClient();

}
