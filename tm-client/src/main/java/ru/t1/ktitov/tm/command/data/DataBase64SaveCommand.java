package ru.t1.ktitov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.ktitov.tm.dto.request.data.DataBase64SaveRequest;

public final class DataBase64SaveCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-save-base64";

    @NotNull
    public static final String DESCRIPTION = "Save data in base64 file";

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull DataBase64SaveRequest request = new DataBase64SaveRequest();
        getDomainEndpointClient().saveDataBase64(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
