package ru.t1.ktitov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.dto.request.AbstractRequest;

import java.io.IOException;
import java.net.Socket;

public interface IEndpointClient {

    @NotNull
    Socket connect() throws IOException;

    @Nullable
    Socket disconnect() throws IOException;

    @NotNull
    Object call(@NotNull final Object object) throws IOException, ClassNotFoundException;

    @NotNull <T> T call(@Nullable final AbstractRequest request, @NotNull Class<T> classResponse) throws IOException, ClassNotFoundException;

}
