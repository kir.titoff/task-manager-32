package ru.t1.ktitov.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.ktitov.tm.api.endpoint.IDomainEndpoint;
import ru.t1.ktitov.tm.dto.request.data.*;
import ru.t1.ktitov.tm.dto.response.data.*;

@NoArgsConstructor
public final class DomainEndpointClient extends AbstractEndpointClient implements IDomainEndpoint {

    public DomainEndpointClient(@NotNull final AbstractEndpointClient client) {
        super(client);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataBackupLoadResponse loadDataBackup(@NotNull final DataBackupLoadRequest request) {
        return call(request, DataBackupLoadResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataBackupSaveResponse saveDataBackup(@NotNull final DataBackupSaveRequest request) {
        return call(request, DataBackupSaveResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataBase64LoadResponse loadDataBase64(@NotNull final DataBase64LoadRequest request) {
        return call(request, DataBase64LoadResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataBase64SaveResponse saveDataBase64(@NotNull final DataBase64SaveRequest request) {
        return call(request, DataBase64SaveResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataBinaryLoadResponse loadDataBinary(@NotNull final DataBinaryLoadRequest request) {
        return call(request, DataBinaryLoadResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataBinarySaveResponse saveDataBinary(@NotNull final DataBinarySaveRequest request) {
        return call(request, DataBinarySaveResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataJsonFasterXmlLoadResponse loadDataJsonFasterXml(@NotNull final DataJsonFasterXmlLoadRequest request) {
        return call(request, DataJsonFasterXmlLoadResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataJsonFasterXmlSaveResponse saveDataJsonFasterXml(@NotNull final DataJsonFasterXmlSaveRequest request) {
        return call(request, DataJsonFasterXmlSaveResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataJsonJaxBLoadResponse loadDataJsonJaxB(@NotNull final DataJsonJaxBLoadRequest request) {
        return call(request, DataJsonJaxBLoadResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataJsonJaxBSaveResponse saveDataJsonJaxB(@NotNull final DataJsonJaxBSaveRequest request) {
        return call(request, DataJsonJaxBSaveResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataXmlFasterXmlLoadResponse loadDataXmlFasterXml(@NotNull final DataXmlFasterXmlLoadRequest request) {
        return call(request, DataXmlFasterXmlLoadResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataXmlFasterXmlSaveResponse saveDataXmlFasterXml(@NotNull final DataXmlFasterXmlSaveRequest request) {
        return call(request, DataXmlFasterXmlSaveResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataXmlJaxBLoadResponse loadDataXmlJaxB(@NotNull final DataXmlJaxBLoadRequest request) {
        return call(request, DataXmlJaxBLoadResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataXmlJaxBSaveResponse saveDataXmlJaxB(@NotNull final DataXmlJaxBSaveRequest request) {
        return call(request, DataXmlJaxBSaveResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataYamlFasterXmlLoadResponse loadDataYamlFasterXml(@NotNull final DataYamlFasterXmlLoadRequest request) {
        return call(request, DataYamlFasterXmlLoadResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataYamlFasterXmlSaveResponse saveDataYamlFasterXml(@NotNull final DataYamlFasterXmlSaveRequest request) {
        return call(request, DataYamlFasterXmlSaveResponse.class);
    }

}
