package ru.t1.ktitov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktitov.tm.dto.request.task.TaskUpdateByIndexRequest;
import ru.t1.ktitov.tm.util.TerminalUtil;

public final class TaskUpdateByIndexCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-update-by-index";

    @NotNull
    public static final String DESCRIPTION = "Update task by index";

    @Override
    public void execute() {
        System.out.println("[UPDATE TASK BY INDEX");
        System.out.print("ENTER INDEX: ");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.print("ENTER NAME: ");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.print("ENTER DESCRIPTION: ");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final TaskUpdateByIndexRequest request = new TaskUpdateByIndexRequest();
        request.setTaskIndex(index);
        request.setName(name);
        request.setDescription(description);
        getTaskEndpointClient().updateByIndex(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
