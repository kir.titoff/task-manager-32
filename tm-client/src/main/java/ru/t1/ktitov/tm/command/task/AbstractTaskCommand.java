package ru.t1.ktitov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.api.endpoint.ITaskEndpoint;
import ru.t1.ktitov.tm.command.AbstractCommand;
import ru.t1.ktitov.tm.enumerated.Role;
import ru.t1.ktitov.tm.enumerated.Status;
import ru.t1.ktitov.tm.exception.entity.TaskNotFoundException;
import ru.t1.ktitov.tm.model.Task;

import java.util.List;

public abstract class AbstractTaskCommand extends AbstractCommand {

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    protected void showTask(@Nullable final Task task) {
        if (task == null) throw new TaskNotFoundException();
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + Status.toName(task.getStatus()));
        System.out.println("PARENT PROJECT: " + task.getProjectId());
    }

    protected void renderTasks(@Nullable final List<Task> tasks) {
        if (tasks == null) throw new TaskNotFoundException();
        int index = 1;
        for (@Nullable final Task task : tasks) {
            if (task == null) continue;
            System.out.println(index + ". " + task);
            index++;
        }
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @NotNull
    protected ITaskEndpoint getTaskEndpointClient() {
        return serviceLocator.getTaskEndpointClient();
    }

}
